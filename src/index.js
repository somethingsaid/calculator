import React from 'react';
import ReactDOM from 'react-dom';
import Calculator from './component/calculator.js';

ReactDOM.render(<Calculator />, document.getElementById('root'));