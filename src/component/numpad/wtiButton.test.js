import React from 'react';
import { waitFor } from '@testing-library/react';
import WTIButton from './wtiButton';
import { unmountComponentAtNode } from "react-dom";
import { mount } from 'enzyme';

let container = null;
let wrapper;
let instance;
let displayOilPrice = jest.fn();

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);

  fetch.resetMocks();

  // get an instance of the component
  wrapper = mount(<WTIButton displayOilPrice={displayOilPrice}/>);
  instance = wrapper.instance();
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('State of oil price', () => {
  it('Should displayOilPrice with successfully fetched price', async () => {
    const oilPriceWebPage = "<table class=\"oilprices__table\"><tbody class=\"row_holder\"><tr class=\"stripe show_graph" +
      " update_on_load\" data-spreadsheet=\"Crude Oil WTI\"><td>Flag</td><td>BuySell</td><td class=\"last_price\" data-price=\"69.66\">69.66</td></tr></tbody></table>";
    fetch.mockResponseOnce(oilPriceWebPage);
    wrapper.simulate('click');
    await waitFor(() => {
      expect(displayOilPrice).toHaveBeenCalledWith("69.66");
    }); 
  });
  it('Should displayOilPrice with error message', async () => {
    fetch.mockRejectOnce({
      status: 403,
      message: "Some error message"
    });
    wrapper.simulate('click');
    await waitFor(() => {
      expect(displayOilPrice).toHaveBeenCalledWith("Some error message");
    }); 
  });
});