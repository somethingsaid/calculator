import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import WTIButton from './wtiButton';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

const commonStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
};

const useStyles = makeStyles((theme) => ({
  numpad: {
    ...commonStyle,
    '& > *': {
      margin: theme.spacing(1),
    },
    minWidth: '700px'
  },
  graphing: {
    ...commonStyle,
    '& > *': {
      margin: theme.spacing(1),
    },
    minWidth: '350px'
  }
}));

const Numpad = ({ onClick, displayOilPrice, mode, draw, disable }) => {
  const classes = useStyles();
  const [graphExpression, setGraphExpression] = useState("4 * sin(x) + 5 * cos(x/2)");

  if (mode === 'basic') {
    return (
      <div className={classes.numpad}>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button disabled={disable} id="button-open-parentheses" onClick={() => onClick("(")}>(</Button>
          <Button disabled={disable} id="button-close-parentheses" onClick={() => onClick(")")}>)</Button>
          <Button disabled={disable} id="button-CE" onClick={() => onClick("CE")}>CE</Button>
          <Button disabled={disable} id="button-C" onClick={() => onClick("C")}>C</Button>
        </ButtonGroup>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button disabled={disable} id="button-1" onClick={() => onClick("1")}>1</Button>
          <Button disabled={disable} id="button-2" onClick={() => onClick("2")}>2</Button>
          <Button disabled={disable} id="button-3" onClick={() => onClick("3")}>3</Button>
          <Button disabled={disable} id="button-plus" onClick={() => onClick("+")}>+</Button>
        </ButtonGroup>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button disabled={disable} id="button-4" onClick={() => onClick("4")}>4</Button>
          <Button disabled={disable} id="button-5" onClick={() => onClick("5")}>5</Button>
          <Button disabled={disable} id="button-6" onClick={() => onClick("6")}>6</Button>
          <Button disabled={disable} id="button-minus" onClick={() => onClick("-")}>-</Button>
        </ButtonGroup>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button disabled={disable} id="button-7" onClick={() => onClick("7")}>7</Button>
          <Button disabled={disable} id="button-8" onClick={() => onClick("8")}>8</Button>
          <Button disabled={disable} id="button-9" onClick={() => onClick("9")}>9</Button>
          <Button disabled={disable} id="button-multiply" onClick={() => onClick("*")}>x</Button>
        </ButtonGroup>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button disabled={disable} id="button-." onClick={() => onClick(".")}>.</Button>
          <Button disabled={disable} id="button-0" onClick={() => onClick("0")}>0</Button>
          <Button disabled={disable} id="button-equals" onClick={() => onClick("=")}>=</Button>
          <Button disabled={disable} id="button-divide" onClick={() => onClick("/")}>÷</Button>
        </ButtonGroup>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button disabled={disable} id="button-exponent" onClick={() => onClick("^")}>^</Button>
          <Button disabled={disable} id="button-sqrt" onClick={() => onClick("sqrt(")}>√</Button>
          <WTIButton displayOilPrice={displayOilPrice} disable={disable} />
          <Button disabled={disable} id="button-mode" onClick={() => onClick("toggle-mode")} size='small' color='secondary'>mode</Button>
        </ButtonGroup>
        <Typography variant="caption" color="inherit">
          Seeing 403s? Request temporary access <a href="https://cors-anywhere.herokuapp.com/corsdemo" target="_blank" rel="noreferrer noopener">here</a>
        </Typography>
      </div>
    );
  } else {
    return (
      <div className={classes.graphing}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="graphing-input"
              label={graphExpression}
              fullWidth={true}
              variant="outlined"
              color="primary"
              helperText="Type your function here"
              onChange={e => {
                setGraphExpression(e.target.value);
              }}
            />
          </Grid>
        </Grid>
        <ButtonGroup color='primary' aria-label='large outlined primary button group' size='large'>
          <Button id="button-mode" onClick={() => draw(graphExpression)}>plot it</Button>
          <Button id="button-mode" onClick={() => onClick("toggle-mode")} color='secondary'>mode</Button>
        </ButtonGroup>
        <Typography variant="caption" color="inherit">Alternate example: x^3 + sqrt(9)</Typography>
      </div>
    );
  }
}

export default Numpad;