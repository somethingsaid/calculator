import Button from '@material-ui/core/Button';
import cheerio from 'cheerio';

const WTIButton = ({ displayOilPrice, disable })=> {
  const corsAnywhere = 'https://cors-anywhere.herokuapp.com/';
  const url = 'https://oilprice.com/oil-price-charts/45';
  const origin = "https://somethingsaid.gitlab.io/calculator";

  const scrapeOilPrice = () => {
    displayOilPrice("fetching");
    fetch(corsAnywhere + url, {
       headers: {
         "Origin": origin,
         "X-Requested-With": "XMLHttpRequest"
       }
      })
      .then(response => {
        if (!response.ok) {
          throw new Error(`Bad network response(${response.status}): ${response.type}`);
        }
        return response.text();
      })
      .then(text => {
        const $ = cheerio.load(text);
        displayOilPrice($("table.oilprices__table tr[data-spreadsheet='Crude Oil WTI'] td.last_price").text());
      })
      .catch(error => {
        displayOilPrice(error.message);
      });
  }

  return (
    <Button id="button-oilprice"
      onClick={scrapeOilPrice}
      variant="outlined"
      color="secondary"
      size="small"
      disabled={disable}
      className="MuiButtonGroup-grouped
        MuiButtonGroup-groupedHorizontal
        MuiButtonGroup-groupedOutlined
        MuiButtonGroup-groupedOutlinedHorizontal
        MuiButtonGroup-groupedOutlinedSecondary">
          WTI
    </Button>
  )
}

export default WTIButton;