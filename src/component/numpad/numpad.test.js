import React from 'react';
import ReactDOM from 'react-dom';
import Numpad from './numpad';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Numpad />, div);
});