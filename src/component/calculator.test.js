import React from 'react';
import ReactDOM from 'react-dom';
import Calculator from './calculator';
import { unmountComponentAtNode } from "react-dom";
import { shallow } from 'enzyme';

let container = null;
let wrapper;
let instance;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);

  // get an instance of the component
  wrapper = shallow(<Calculator />);
  instance = wrapper.instance();
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Calculator />, div);
});

describe('main methods', () => {
  describe("displayOilPrice", () => {
    it('should set oil price', () => {
      expect(instance.state.operation).toBe("");
      instance.displayOilPrice("45.12");
      expect(instance.state.operation).toBe("45.12");
    });
    it('should replace expression with error message', () => {
      instance.setState({operation: "102+1"});
      instance.displayOilPrice("Bad network response");
      expect(instance.state.operation).toBe("Bad network response");
    });
  });

  it('should reset state', () => {
    instance.setState({operation: "102+1", result: "103"});
    instance.reset();
    expect(instance.state).toEqual({operation: "", result: "", mode: "basic", fetchingData: false});
  });

  it('should evaluate an expression properly', () => {
    const actual = instance.eval("1+sqrt(81)^3");
    expect(actual).toBe(730);
  });
  
  it('should backspace', () => {
    instance.setState({operation: "123+4"});
    instance.backspace();
    expect(instance.state.operation).toBe("123+");
  });
  
  describe('calculate', () => {
    it('should set the result for a valid expression', () => {
      instance.setState({operation: "1 + 2 + 3 + 4"});
      instance.calculate();
      expect(instance.state.result).toBe(10);
    });
    it('should set an error result for an invalid expression', () => {
      instance.setState({operation: "1+++++"});
      instance.calculate();
      expect(instance.state.result).toBe("error");
    });
  });


  describe('onClick', () => {
    it('should call calculate', () => {
      const calculateSpy = jest.spyOn(instance, 'calculate');
      instance.onClick("=");
      expect(calculateSpy).toHaveBeenCalled();
    });
    it('should call backspace', () => {
      const backspaceSpy = jest.spyOn(instance, 'backspace');
      instance.onClick("CE");
      expect(backspaceSpy).toHaveBeenCalled();
    });
    it('should call reset', () => {
      const resetSpy = jest.spyOn(instance, 'reset');
      instance.onClick("C");
      expect(resetSpy).toHaveBeenCalled();
    });
    it('should update operation with any other input', () => {
      const randomInput = "@@@@@@@@@";
      instance.onClick(randomInput);
      expect(instance.state.operation).toBe(randomInput);
    });
  });
})