import React from 'react';
import TextField from '@material-ui/core/TextField';

const DisplayRow = ({ id, value })=> {
  return (
    <TextField
      id={id}
      label={value}
      inputProps={{readOnly: true}}
      disabled
      fullWidth={true}
      variant="outlined" />
  )
}

export default DisplayRow;