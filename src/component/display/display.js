import React from 'react';
import DisplayRow from './displayRow.js';
import Grid from '@material-ui/core/Grid';

const Display = ({ operation, result, mode })=> {
  if (mode === 'basic') {
    return (
      <div style={{textAlign: 'center', width: '100%'}}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <DisplayRow id="display-operation" value={operation} />
          </Grid>
          <Grid item xs={12}>
            <DisplayRow id="display-result" value={result} />
          </Grid>
        </Grid>
      </div>
    );
  } else {
    return (
      <div style={{textAlign: 'center', height: '200px', width: '100%', border: '1px solid grey', borderRadius: '4px'}} id='plot'> </div>
    );
  }
}

export default Display;