import React from 'react';
import ReactDOM from 'react-dom';
import DisplayRow from './displayRow';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DisplayRow />, div);
});