import React, { Component } from 'react';
import Display from './display/display';
import Numpad from './numpad/numpad';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Typography } from '@material-ui/core';
import { create, all } from 'mathjs';
import Plotly from 'plotly.js-dist';

class Calculator extends Component {
  constructor() {
    super();
    this.state = {
      operation: "",
      result: "",
      mode: "basic",
      fetchingData: false
    }
    this.math = create(all, {});
  }

  eval = expression => {
      const result = this.math.evaluate(expression);
      return result === 0 ? "0" : result;
  };

  calculate = () => {
    try {
      this.setState({
        result: this.eval(this.state.operation)
      });
    } catch (e) {
      this.setState({
        result: "error"
      });
    }
  };

  reset = () => {
    this.setState({
      operation: "",
      result: ""
    });
  }

  backspace = () => {
    this.setState({
      operation: this.state.operation.slice(0, -1)
    });
  };

  draw = graphExpression => {
    try {
      // compile the expression once
      const expr = this.math.compile(graphExpression);

      // evaluate the expression repeatedly for different values of x
      const xValues = this.math.range(-10, 10, 0.5).toArray();
      const yValues = xValues.map(function (x) {
        return expr.evaluate({x: x})
      });

      // render the plot using plotly
      const layout = {
        autosize: true,
        margin: {
          l: 15,
          r: 15,
          b: 15,
          t: 15,
        }
      };
      const trace1 = {
        x: xValues,
        y: yValues,
        type: 'scatter'
      };
      const data = [trace1];
      Plotly.newPlot('plot', data, layout);
    }
    catch (err) {
      console.error(err);
      alert(err);
    }
  }

  onClick = (button) => {
    switch (button) {
      case "=":
        this.calculate();
        break;
      case "C":
        this.reset();
        break;
      case "CE":
        this.backspace();
        break;
      case "toggle-mode":
        this.setState({
          mode: this.state.mode === "basic" ? "graphing" : "basic"
        });
        break;
      default:
        this.setState({
          operation: this.state.operation + button
        });
    }
  };

  displayOilPrice = oilPrice => {
    if (oilPrice === "fetching") {
      this.setState({
        fetchingData: true
      });
    } else if (oilPrice.startsWith("Bad network response")) {
      this.setState({
        operation: oilPrice,
        fetchingData: false
      });
    } else {
      this.setState({
        operation: this.state.operation + oilPrice,
        fetchingData: false
      });
    }
  }

  render() {
    return (
      <div>
        <AppBar color="primary" position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit">Basic Calculator</Typography>
          </Toolbar>
        </AppBar>
        <div style= {{marginTop: 5}}>
          <div style={{height: "5px"}}>
            {this.state.fetchingData ? <LinearProgress color="secondary" /> : null}
          </div>
          <Grid container spacing={0} justify="center">
            <Display operation={this.state.operation} result={this.state.result} mode={this.state.mode} />
            <Numpad
              onClick={this.onClick}
              displayOilPrice={this.displayOilPrice}
              mode={this.state.mode}
              draw={this.draw}
              disable={this.state.fetchingData}
            />
          </Grid>
        </div>
      </div>
    )
  }
}

export default Calculator;