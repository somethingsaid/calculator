describe("Basic layout for calculator app", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Should display the correct header", () => {
    cy.get("h6").should("exist").and("have.text", "Basic Calculator");
  });

  it("Should have two inputs", () => {
    cy.get("input").should("have.length", 2);
  });

  it("Should have six ButtonGroups", () => {
    cy.get(".MuiButtonGroup-root").should("have.length", 6);
  });
});