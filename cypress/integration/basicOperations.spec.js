describe("Operations for calculator app", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  describe("Basic operations: + - * /", () => {
    it("Should add correctly", () => {
      cy.get("#button-1").click();
      cy.get("#button-plus").click();
      cy.get("#button-5").click();
      cy.get("#button-equals").click();
  
      cy.get("#display-operation-label").should("have.text", "1+5");
      cy.get("#display-result-label").should("have.text", "6");
    });
  
    it("Should subtract correctly", () => {
      cy.get("#button-9").click();
      cy.get("#button-8").click();
      cy.get("#button-minus").click();
      cy.get("#button-1").click();
      cy.get("#button-0").click();
      cy.get("#button-5").click();
      cy.get("#button-equals").click();
  
      cy.get("#display-operation-label").should("have.text", "98-105");
      cy.get("#display-result-label").should("have.text", "-7");
    });
  
    it("Should multiply correctly", () => {
      cy.get("#button-9").click();
      cy.get("#button-multiply").click();
      cy.get("#button-3").click();
      cy.get("#button-equals").click();
  
      cy.get("#display-operation-label").should("have.text", "9*3");
      cy.get("#display-result-label").should("have.text", "27");
    });
    
    it("Should divide correctly", () => {
      cy.get("#button-1").click();
      cy.get("#button-0").click();
      cy.get("#button-divide").click();
      cy.get("#button-3").click();
      cy.get("#button-equals").click();
  
      cy.get("#display-operation-label").should("have.text", "10/3");
      cy.get("#display-result-label").invoke('text').should("match", /^3.3333333/);
    });
    
    it("Should do BEDMAS correctly", () => {
      cy.get("#button-1").click();
      cy.get("#button-0").click();
      cy.get("#button-divide").click();
      cy.get("#button-5").click();
      cy.get("#button-exponent").click();
      cy.get("#button-3").click();
      cy.get("#button-multiply").click();
      cy.get("#button-open-parentheses").click();
      cy.get("#button-sqrt").click();
      cy.get("#button-9").click();
      cy.get("#button-close-parentheses").click();
      cy.get("#button-minus").click();
      cy.get("#button-2").click();
      cy.get("#button-close-parentheses").click();
      cy.get("#button-equals").click();
  
      cy.get("#display-operation-label").should("have.text", "10/5^3*(sqrt(9)-2)");
      cy.get("#display-result-label").should("have.text", "0.08");
    });

    it("Should display an error", () => {
      cy.get("#button-4").click();
      cy.get("#button-sqrt").click();
      cy.get("#button-equals").click();
      cy.get("#display-result-label").should("have.text", "error");
    });
  });

  describe("Special operations: backspace, clear all", () => {
    it('Should remove last entry', () => {
      cy.get("#button-1").click();
      cy.get("#button-plus").click();
      cy.get("#button-sqrt").click();
      cy.get("#button-5").click();
      cy.get("#display-operation-label").should("have.text", "1+sqrt(5");

      cy.get("#button-CE").click();
      cy.get("#display-operation-label").should("have.text", "1+sqrt(");
    });
    
    it("Should clear all input", () => {
      cy.get("#button-1").click();
      cy.get("#button-plus").click();
      cy.get("#button-sqrt").click();
      cy.get("#button-9").click();
      cy.get("#button-close-parentheses").click();
      cy.get("#button-equals").click();

      cy.get("#display-operation-label").should("have.text", "1+sqrt(9)");
      cy.get("#display-result-label").should("have.text", "4");

      cy.get("#button-C").click();
      cy.get("#display-operation-label").should("not.exist");
      cy.get("#display-result-label").should("not.exist");
    });
  });
  
  describe("Fetching WTI Oil Price", () => {
      const corsAnywhere = 'https://cors-anywhere.herokuapp.com/';
      const url = 'https://oilprice.com/oil-price-charts/45'
    it('Should display WTI crude oil price', () => {
      cy.intercept(corsAnywhere + url, { fixture: "oilPriceCharts.json"});
      cy.get("#button-oilprice").click();
      cy.get("#display-operation-label").should("have.text", "69.62");
    });
  });
});