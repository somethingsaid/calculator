# Calculator App
Deployed at: https://somethingsaid.gitlab.io/calculator/ \
Made possible thanks to [CORS Anywhere](https://cors-anywhere.herokuapp.com/), [MathJs](https://mathjs.org/), and [Plotly](https://github.com/plotly/plotly.js).

## Troubleshooting network problems
Scraping the HTML of [WTI Crude Oil Price](https://oilprice.com/oil-price-charts/45) is done with [CORS Anywhere](https://cors-anywhere.herokuapp.com/).\
If you're seeing network problems (i.e. `403 Forbidden`), you may have request temporary access [here](https://cors-anywhere.herokuapp.com/corsdemo).


## TODOs
- [x] Button to parse price of WTI from https://oilprice.com/oil-price-charts/45
  - [x] Resolve response currently responding with 403 Forbidden.
  - [ ] Would be nice to cache the oil price in memory, and only make new web request after some time interval or at Nth WTI button press
- [ ] Graphical calculator feature
  - [x] Button to toggle Display to a canvas for graph
  - [x] Same toggle will toggle Numpad to graphing calculator layout or text put for function?
  - [ ] Add tests for graphical calculator/plotting
  - [ ] Replace text input with button input (check out physical TI series graphing calculators for reference)
- [ ] Tighten up Styling - work on theming, responsiveness, alternate numpad layout for landscape mode, etc.
- [ ] Optimize pipeline
- [ ] QA/test on other browsers.  For example, DuckDuckGo mobile browser on Nexus 5 isn't rendering the calculator
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`
Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run test-coverage`
Run tests with coverage report

### E2E test
With the app up at [http://localhost:3000](http://localhost:3000), run `npm run cy-open` to launch the Cypress test runner. Select the tests you want to run.

## Screenshots
![Basic](images/basic.png)
![Graphing](images/graphing.png)
